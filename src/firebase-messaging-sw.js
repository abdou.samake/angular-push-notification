importScripts('https://www.gstatic.com/firebasejs/7.6.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.6.0/firebase-messaging.js');

firebase.initializeApp({
  apiKey: "AIzaSyBLaB0vwlziJ3i3viNwSblLwlymlUYFn0k",
  authDomain: "notification-23b39.firebaseapp.com",
  databaseURL: "https://notification-23b39.firebaseio.com",
  projectId: "notification-23b39",
  storageBucket: "notification-23b39.appspot.com",
  messagingSenderId: "793351611601",
  appId: "1:793351611601:web:5beaa23ce049de18ebd1f0"
});

const messaging = firebase.messaging();
