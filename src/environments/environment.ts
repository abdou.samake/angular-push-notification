// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyBLaB0vwlziJ3i3viNwSblLwlymlUYFn0k',
    authDomain: 'notification-23b39.firebaseapp.com',
    databaseURL: 'https://notification-23b39.firebaseio.com',
    projectId: 'notification-23b39',
    storageBucket: 'notification-23b39.appspot.com',
    messagingSenderId: '793351611601',
    appId: '1:793351611601:web:5beaa23ce049de18ebd1f0'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
